<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index(){
		$data = array();
		$data['main_view'] = 'begin';
		//$this->load->view('template', $data);
		$this->load->view('hello');
	}
	
	public function generate(){
		$inputan = $this->input->post('shooping');
		//$this->tokopedia($inputan);

		$data['bukalapak'] = $this->bukalapak($inputan);
		$data['blibli'] = $this->blibli($inputan);
		
		$data['toko'] = array('bukalapak', 'blibli');
		$data['main_view'] = 'result';
		$this->load->view('template', $data);
	}
	
	public function blibli($inputan){
		$link = "https://www.blibli.com";
		$page = $this->jualan->curl($link);
		$cookie = $this->jualan->GetCookies($page) . "; ";
		
		$input_cari = $inputan;
		$input_cari = str_replace(' ', '+', $input_cari);
		
		$linkcari = "https://www.blibli.com/search?s=".$input_cari;
		$page = $this->jualan->curl($linkcari, $cookie, 0, $link);
		$cookie = $this->jualan->GetCookies($page) . "; ";
		
		if(stristr($page, "Location: ")){
			preg_match('/Location: (.+?)\r\n/', $page, $match);
			$link = $match[1];
			$page = $this->jualan->curl($link, $cookie, 0, $linkcari);
			$cookie = $this->jualan->GetCookies($page) . "; ";
		}
		
		preg_match_all('/<a class="single-product" href="(.+?)">/', $page, $link_jualan);
		preg_match_all('/<span class="new-price-text">(.+?)<\/span>/', $page, $harga);
		preg_match_all('/<img class="lazy" alt="(.+?)" title="(.+?)" data-original="(.+?)"/', $page, $gambar);
		
		$blibli = array();
		$blibli['blibli']['judul'] = $gambar[2];
		$blibli['blibli']['link'] = $link_jualan[1];
		$blibli['blibli']['harga'] = $harga[1];
		$blibli['blibli']['gambar'] = $gambar[3];
		
		return $blibli;
	}
	
	function bukalapak($inputan){

		$inputan = str_replace(" ", "+", $inputan);
		
		$linkcari = "https://www.bukalapak.com/products?utf8=%E2%9C%93&search%5Bkeywords%5D=".$inputan;
		$referer = "https://www.bukalapak.com/";
		
		//get halaman bukalapak
		$page = $this->jualan->curl($referer);
		$cookie = $this->jualan->GetCookies($page) . "; ";
		
		//get search
		$page = $this->jualan->curl($linkcari, $cookie, 0, $referer);
		$cookie = $this->jualan->GetCookies($page) . "; ";
		
		//get data
		preg_match_all('/<a href=\'(.+?)\' title=\'(.+?)\'>/', $page, $link); //link 1 judul 2
		preg_match_all('/<span class=\'amount positive\'>(.+?)<\/span><\/span>/', $page, $harga); //harga 1
		preg_match_all('/<img alt="(.+?)" src="(.+?)" \/>/', $page, $gambar); //gambar 1 gambar 2

		//result
		$bukalapak = array();
		$bukalapak['bukalapak']['judul'] = $link[2];
		$bukalapak['bukalapak']['link'] = $link[1];
		$bukalapak['bukalapak']['harga'] = $harga[1];
		$bukalapak['bukalapak']['gambar'] = $gambar[2];

		return $bukalapak;
	}
	
	function tokopedia($inputan){
		$inputan = str_replace(" ", "+", $inputan);
		
		//$linkcari = "https://www.tokopedia.com/search?q=".$inputan."&sc=0&st=product";
		$linkcari = "http://www.tokopedia.com/search?q=".$inputan."&utm_source=ops&utm_medium=wb&utm_campaign=OpenSearch";

		$referer = "https://www.tokopedia.com";

		//get halaman tokopedia
		$page = $this->jualan->curl($referer);
		$cookie = $this->jualan->GetCookies($page) ."; ";
		
		$this->jualan->debug($page);
		$this->jualan->debug($linkcari);
		//get halaman serach
		$page = $this->jualan->curl($linkcari, $cookie, 0, $referer);
		$cookie = $this->jualan->GetCookies($page) .";" ;

		$this->jualan->debug($page);
		if(stristr($page, 'Location: ')){
			preg_match('/Location: (.+)/', $page, $location);
			print_r($location);

			$page = $this->jualan->curl($location[1], $cookie);

			$this->jualan->debug($page);
		}
		exit();




		
	}
	

}
